package com.liduxian.api.util;

import java.util.Arrays;
import java.util.Objects;

public class MethodCacheKey {

	private final String methodName;
	private final String[] paramTypeName;
	private final String declareClassName;
	private final int hashCode;

	public MethodCacheKey(String methodName,String declareClassName,String...paramTypeName){
		this.methodName=methodName;
		this.declareClassName=declareClassName;
		this.paramTypeName=paramTypeName;
		int result = Objects.hash(methodName, declareClassName);
		result = 31 * result + Arrays.hashCode(paramTypeName);
		hashCode=result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		MethodCacheKey that = (MethodCacheKey) o;
		return Objects.equals(methodName, that.methodName) && Arrays.equals(paramTypeName, that.paramTypeName) && Objects.equals(declareClassName, that.declareClassName);
	}

	@Override
	public int hashCode() {
		return this.hashCode;
	}
}
