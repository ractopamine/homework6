package com.liduxian.api.protocal;

public class Status {
	public static final String SUCCESS="200";
	public static final String ILLEGAL_PARAMS="400";
	public static final String NO_RESOURCES_FOUND="404";
}
