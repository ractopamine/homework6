package com.liduxian.api.service;

import com.liduxian.api.anno.RpcMapping;
import com.liduxian.api.pojo.User;

@RpcMapping("/userService")
public interface IUserService {

	@RpcMapping("/getUserById")
	User getUserById(Integer id);
}
