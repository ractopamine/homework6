package com.liduxian.api.protocal;

import java.io.Serializable;

public class Request implements Serializable {
	private String url;
	private Object[] params;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Object[] getParams() {
		return params;
	}

	public void setParams(Object[] params) {
		this.params = params;
	}
}
