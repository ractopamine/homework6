package com.liduxian.api.anno;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Component
@Target(ElementType.TYPE)
public @interface RpcService {


}
