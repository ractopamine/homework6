package com.liduxian.api.protocal;

import java.lang.reflect.Method;

public class MappingHandler {

	private final String url;

	private final Method method;

	private final Object target;

	public MappingHandler(String url, Method method, Object target) {
		this.url = url;
		this.method = method;
		this.target = target;
	}

	public String getUrl() {
		return url;
	}

	public Method getMethod() {
		return method;
	}

	public Object getTarget() {
		return target;
	}
}
