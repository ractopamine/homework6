package com.liduxian.api.protocal;

import java.io.Serializable;

public class Response implements Serializable {
	private String code;

	private Object result;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}
}
