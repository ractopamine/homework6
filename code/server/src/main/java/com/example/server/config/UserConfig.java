package com.example.server.config;

import com.liduxian.api.pojo.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class UserConfig {

	@Value("${name}")
	private String name;

	@Value("${id}")
	private Integer id;

	@Bean("userData")
	public Map<Integer,User> userData(){
		Map<Integer, User> map = new HashMap<>();
		User user = new User();
		user.setUsername(name);
		user.setId(id);
		map.put(id,user);
		return map;
	}

}
