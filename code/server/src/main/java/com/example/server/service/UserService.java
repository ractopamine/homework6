package com.example.server.service;

import com.liduxian.api.anno.RpcMapping;
import com.liduxian.api.anno.RpcService;
import com.liduxian.api.pojo.User;
import com.liduxian.api.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

@RpcService
@RpcMapping("/userService")
public class UserService implements IUserService {

	@Autowired
	private Map<Integer,User> userData;

	@Override
	@RpcMapping("/getUserById")
	public User getUserById(Integer id) {
		return userData.get(id);
	}

}
