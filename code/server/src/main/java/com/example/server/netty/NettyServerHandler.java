package com.example.server.netty;

import com.alibaba.fastjson.JSON;
import com.liduxian.api.anno.RpcMapping;
import com.liduxian.api.anno.RpcService;
import com.liduxian.api.protocal.MappingHandler;
import com.liduxian.api.protocal.Request;
import com.liduxian.api.protocal.Response;
import com.liduxian.api.protocal.Status;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.SocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

@Component
@ChannelHandler.Sharable
public class NettyServerHandler extends ChannelInboundHandlerAdapter implements ApplicationContextAware {
	@Autowired
	@Qualifier("taskHandlers")
	private ThreadPoolExecutor taskHandlers;
	/**
	 * 专门处理服务集合;
	 */
	private Map<String, MappingHandler> services;

	private String getTypeMapping(Object object){
		RpcMapping annotation = object.getClass().getAnnotation(RpcMapping.class);
		Assert.notNull(annotation,"无法暴露服务,缺少rpcMaping");
		return annotation.value();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		services=new HashMap<>();
		Map<String, Object> beans = applicationContext.getBeansWithAnnotation(RpcService.class);
		beans.forEach((k,v)->{
			String typeMapping = getTypeMapping(v);
			Method[] declaredMethods = v.getClass().getDeclaredMethods();
			for (Method declaredMethod : declaredMethods) {
				if (declaredMethod.isAnnotationPresent(RpcMapping.class)){
					RpcMapping annotation = declaredMethod.getAnnotation(RpcMapping.class);
					String methodUrl = annotation.value();
					String url=typeMapping + methodUrl;
					Assert.isTrue(!services.containsKey(url),"重复映射");
					services.put(url,new MappingHandler(url,declaredMethod,v));
				}
			}
		});
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		taskHandlers.execute(()-> doRead(ctx,msg));
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		SocketAddress socketAddress = ctx.channel().remoteAddress();
		System.out.println("新的连接:"+socketAddress);
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		SocketAddress socketAddress = ctx.channel().remoteAddress();
		System.out.println("连接关闭:"+socketAddress);
	}

	public void doRead(ChannelHandlerContext ctx, Object msg){
		Request request = JSON.parseObject((String) msg, Request.class);
		Response response = new Response();
		String url = request.getUrl();
		if (services.containsKey(url)){
			MappingHandler mappingHandler = services.get(url);
			Method method = mappingHandler.getMethod();
			Object target = mappingHandler.getTarget();
			Object[] params = request.getParams();
			try {
				response.setResult(method.invoke(target, params));
				response.setCode(Status.SUCCESS);
			} catch (IllegalAccessException | InvocationTargetException e) {
				response.setCode(Status.ILLEGAL_PARAMS);

			}
		}else {
			response.setCode(Status.NO_RESOURCES_FOUND);
		}
		String resString = JSON.toJSONString(response);
		ctx.writeAndFlush(resString);
	}
}
