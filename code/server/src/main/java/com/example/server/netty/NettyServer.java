package com.example.server.netty;

import com.example.server.config.NettyConfig;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;


public class NettyServer implements Runnable {

	private ServerBootstrap strap;

	private NioEventLoopGroup boss;

	private NioEventLoopGroup worker;

	private final NettyConfig nettyConfig;

	private final NettyServerHandler nettyServerHandler;

	public NettyServer(NettyConfig nettyConfig,NettyServerHandler nettyServerHandler){
		this.nettyConfig=nettyConfig;
		this.nettyServerHandler=nettyServerHandler;
	}

	public void close(){
		boss.shutdownGracefully();
		worker.shutdownGracefully();
	}

	public void init() {
		strap=new ServerBootstrap();
		boss=new NioEventLoopGroup(1);
		worker=new NioEventLoopGroup();
		strap.group(boss,worker);
		strap.option(ChannelOption.SO_BACKLOG,1024)
				.channel(NioServerSocketChannel.class)
				.childOption(ChannelOption.SO_KEEPALIVE,Boolean.TRUE)
				.childHandler(new ChannelInitializer<SocketChannel>() {
					@Override
					protected void initChannel(SocketChannel socketChannel) throws Exception {
						socketChannel.pipeline().addLast(new StringDecoder());
						socketChannel.pipeline().addLast(new StringEncoder());
						socketChannel.pipeline().addLast(nettyServerHandler);
					}
				});
	}

	@Override
	public void run() {
		try {
			ChannelFuture sync = strap.bind(nettyConfig.getPort()).sync();
			System.out.println("服务启动成功,绑定端口:"+ nettyConfig.getPort());
			sync.channel().closeFuture().sync();
			System.out.println("服务正在关闭.....");
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}finally {
			boss.shutdownGracefully();
			worker.shutdownGracefully();
		}
	}
}
