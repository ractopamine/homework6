package com.example.server.config;

import com.example.server.netty.NettyServer;
import com.example.server.netty.NettyServerHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.*;

@Configuration
public class TaskExecutorConfig{
	@Value("${netty.handler.corePoolSize}")
	private int corePoolSize;
	@Value("${netty.handler.maximumPoolSize}")
	private int maximumPoolSize;
	@Value("${netty.handler.keepAliveTime}")
	private long keepAliveTime;
	@Value("${netty.handler.capacity}")
	private int capacity;
	@Autowired
	private NettyConfig nettyConfig;
	@Autowired
	private NettyServerHandler nettyServerHandler;

	@Bean("taskHandlers")
	public ThreadPoolExecutor threadPoolExecutor(){
		ArrayBlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(capacity);

		return new ThreadPoolExecutor(
				corePoolSize,
				maximumPoolSize,
				keepAliveTime,
				TimeUnit.MILLISECONDS,
				queue,
				new ThreadPoolExecutor.CallerRunsPolicy()
		);
	}

	@Bean(initMethod = "init",destroyMethod = "close")
	public NettyServer nettyServer(){
		return new NettyServer(nettyConfig,nettyServerHandler);
	}


}
