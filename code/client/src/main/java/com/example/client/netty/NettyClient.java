package com.example.client.netty;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

public class NettyClient{

	private final NettyClientSyncHandler nettyClientHandler;

	private NioEventLoopGroup worker;

	private Bootstrap bootstrap;

	private ChannelFuture channelFuture;

	private final String address;

	public NettyClient(String address,NettyClientSyncHandler nettyClientHandler){
		this.address=address;
		this.nettyClientHandler=nettyClientHandler;
	}


	public void init(){
		bootstrap = new Bootstrap();
		worker=new NioEventLoopGroup(1);
		bootstrap.group(worker);
		bootstrap.channel(NioSocketChannel.class)
				.handler(new ChannelInitializer<SocketChannel>() {
					@Override
					protected void initChannel(SocketChannel socketChannel) throws Exception {
						socketChannel.pipeline().addLast(new StringDecoder());
						socketChannel.pipeline().addLast(new StringEncoder());
						socketChannel.pipeline().addLast(nettyClientHandler);
					}
				});


		String[] splitAddr = address.split(":");
		channelFuture= bootstrap.connect(splitAddr[0], Integer.parseInt(splitAddr[1]));
	}
	//得到一个
	public ChannelFuture connect(){
		return channelFuture;
	}


	public void destroy(){
		System.out.println("客户端正在关闭");
		worker.shutdownGracefully();
	}
}
