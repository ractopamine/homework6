package com.example.client.service;

import com.liduxian.api.pojo.User;

public interface ClientUserService {

	User getUserById(Integer id);
}
