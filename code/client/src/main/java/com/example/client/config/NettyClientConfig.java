package com.example.client.config;

import com.example.client.netty.NettyClient;
import com.example.client.netty.NettyClientSyncHandler;
import com.example.client.netty.NettyClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Configuration
public class NettyClientConfig {

	@Autowired
	private RpcConfig rpcConfig;
	@Autowired
	private NettyClientSyncHandler nettyClientHandler;

	@Bean(initMethod = "init",destroyMethod = "destroy")
	public NettyClients nettyClients(){
		String[] address = rpcConfig.getAddress();
		List<NettyClient> list = new ArrayList<>(address.length);

		for (String addr : address) {
			list.add(new NettyClient(addr,nettyClientHandler));
		}

		return  new NettyClients(list);
	}
}
