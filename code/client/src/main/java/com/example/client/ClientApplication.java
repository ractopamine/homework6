package com.example.client;

import com.example.client.config.RemoteSyncProxy;
import com.example.client.netty.NettyClients;
import com.liduxian.api.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ClientApplication {

	@Autowired
	private NettyClients nettyClients;


	public static void main(String[] args) {
		SpringApplication.run(ClientApplication.class, args);
	}

	@Bean
	public IUserService userService(){
		return RemoteSyncProxy.proxy(IUserService.class, nettyClients);
	}

}
