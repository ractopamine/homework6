package com.example.client.service;

import com.liduxian.api.pojo.User;
import com.liduxian.api.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientUserServiceImpl implements ClientUserService{

	@Autowired
	private IUserService iUserService;

	@Override
	public User getUserById(Integer id) {
		User userById = iUserService.getUserById(id);
		System.out.println("远端调用结果为:"+userById);
		return userById;
	}
}
