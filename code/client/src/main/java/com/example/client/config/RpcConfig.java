package com.example.client.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RpcConfig {

	@Value("${rpc.remote.service.address}")
	private String[] address;

	public void setAddress(String[] address) {
		this.address = address;
	}

	public String[] getAddress() {
		return address;
	}
}
