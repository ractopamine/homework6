package com.example.client.netty;

import io.netty.channel.Channel;

import java.util.concurrent.ConcurrentHashMap;

public class NettySyncContext {

	private static final Object resultHolder=null;
	/**
	 * 通信注册表;
	 */
	private static final ConcurrentHashMap<Channel,NettySyncBag> syncTable=new ConcurrentHashMap<>();

	/**
	 * 注册
	 * @param channel 通行channel
	 */
	public static void registered(Channel channel){
		NettySyncBag nettySyncBag = new NettySyncBag();
		nettySyncBag.setChannel(channel);
		nettySyncBag.setParkingThread(Thread.currentThread());
		nettySyncBag.setResult(resultHolder);
		syncTable.put(channel, nettySyncBag);
	}

	/**
	 * 注销
	 */
	public static void remove(Channel channel){
		syncTable.remove(channel);
	}

	/**
	 * 查询结果;
	 */
	public static NettySyncBag get(Channel channel){
		return syncTable.get(channel);
	}

	public static class NettySyncBag{
		//parking 的线程;响应成功过后唤醒
		private Thread parkingThread;
		//netty 通行的channel
		private Channel channel;
		//通信的结果;
		private volatile Object result;

		public Thread getParkingThread() {
			return parkingThread;
		}

		public void setParkingThread(Thread parkingThread) {
			this.parkingThread = parkingThread;
		}

		public Channel getChannel() {
			return channel;
		}

		public void setChannel(Channel channel) {
			this.channel = channel;
		}

		public Object getResult() {
			return result;
		}

		public void setResult(Object result) {
			this.result = result;
		}
	}

}
