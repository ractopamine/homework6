package com.example.client.config;

import com.alibaba.fastjson.JSON;
import com.example.client.netty.NettyClient;
import com.example.client.netty.NettyClients;
import com.example.client.netty.NettySyncContext;
import com.liduxian.api.anno.RpcMapping;
import com.liduxian.api.protocal.Request;
import com.liduxian.api.protocal.Response;
import io.netty.channel.ChannelFuture;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;
import org.springframework.util.Assert;

import java.lang.reflect.Method;
import java.util.concurrent.locks.LockSupport;

public class RemoteSyncProxy implements MethodInterceptor {

	private final Class<?> remoteInterface;
	private final NettyClients clients;

	private RemoteSyncProxy(NettyClients clients,Class<?> remoteInterface){
		this.clients=clients;
		this.remoteInterface=remoteInterface;
	}

	@Override
	public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
		NettyClient client = clients.get();

		ChannelFuture connect = client.connect();
		if (!connect.channel().isActive()){
			throw new RuntimeException(" channel is not active");
		}
		synchronized (connect){
			Request request = new Request();
			String url= getInvokeUrl(method);
			request.setUrl(url);
			request.setParams(objects);
			String s = JSON.toJSONString(request);
			connect.channel().writeAndFlush(s);
			try {
				//注册请求表;
				NettySyncContext.registered(connect.channel());
				//parked
				LockSupport.park();

				NettySyncContext.NettySyncBag bag = NettySyncContext.get(connect.channel());
				Response response = JSON.parseObject((String) bag.getResult(), Response.class);
				return JSON.parseObject(response.getResult().toString(),method.getReturnType());
			}finally {
				NettySyncContext.remove(connect.channel());
			}
		}
	}

	private String getInvokeUrl(Method method) {
		RpcMapping methodMapping = method.getAnnotation(RpcMapping.class);
		RpcMapping typeMapping = remoteInterface.getAnnotation(RpcMapping.class);
		Assert.notNull(methodMapping,"无法找到方法映射");
		Assert.notNull(typeMapping,"无法找到类的映射");
		return typeMapping.value()+methodMapping.value();
	}


	@SuppressWarnings("unchecked")
	public static <T> T  proxy(Class<T> remoteInterface, NettyClients nettyClients){
		RemoteSyncProxy remoteProxy = new RemoteSyncProxy(nettyClients,remoteInterface);
		return  (T)Enhancer.create(remoteInterface, remoteProxy);
	}
}
