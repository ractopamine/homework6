package com.example.client.netty;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.springframework.stereotype.Component;

import java.util.concurrent.locks.LockSupport;

@Component
@ChannelHandler.Sharable
public class NettyClientSyncHandler extends ChannelInboundHandlerAdapter {

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		//同步通道
		Channel channel = ctx.channel();
		NettySyncContext.NettySyncBag nettySyncBag = NettySyncContext.get(channel);
		nettySyncBag.setResult(msg);
		Thread parkingThread = nettySyncBag.getParkingThread();

		LockSupport.unpark(parkingThread);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
	}
}
