package com.example.client.cotroller;


import com.example.client.service.ClientUserService;
import com.liduxian.api.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private ClientUserService clientUserService;

	@GetMapping("/getUser")
	public User getUser(Integer id){
		return clientUserService.getUserById(id);
	}
}
