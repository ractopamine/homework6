package com.example.client.netty;

import org.springframework.util.Assert;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class NettyClients {

	private final AtomicLong counter=new AtomicLong(0);

	private final List<NettyClient> clients;

	public NettyClients(List<NettyClient> clients){
		Assert.notNull(clients,"注册实例不可为空");
		this.clients=clients;
	}

	public NettyClient get(){
		int mode = (int)(counter.addAndGet(1)) & (clients.size()-1);
		return clients.get(mode);
	}

	public void init(){
		for (NettyClient client : clients) {
			client.init();
		}
	}

	public void destroy(){
		for (NettyClient client : clients) {
			client.destroy();
		}
	}
}
